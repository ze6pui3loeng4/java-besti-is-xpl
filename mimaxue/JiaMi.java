public class JiaMi {
    static String Encrypt(String mingwen, int key){
        char[] plaintext=mingwen.toCharArray();
        String ciphertext;
        int i;
        int cipher;
        for(i=0;i<plaintext.length;i++){
            if( (plaintext[i]>='a' && plaintext[i]<='z') || (plaintext[i]>='A' && plaintext[i]<='Z') ){
                cipher=(plaintext[i]+key)%26;
                if(cipher==0) {
                    cipher=cipher+26;
                }
                plaintext[i]=(char)cipher;
            }
            else{
                return new String("错误！");
            }
        }
        ciphertext=String.valueOf(plaintext);
        return new String(ciphertext);
    }
}

