第一周学习总结
==
这是开学之后的第一周，我们终于开始上Java的课程了。第一周，我们的任务是学习Linux、Ubuntu的命令，以及初步开始编写Java程序，和熟悉git bash的安装和使用。
## 一、Ubuntu命令学习
按照老师发的教程初步学习了一些Linux的命令：
![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304220537947-1497889495.png)
![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304220547029-1557723180.png)
![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304220551729-861069491.png)
## 二、git安装
一开始，在虚拟机中安装git遇到了各种问题：
![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304220605983-686463021.png)
但后来还是解决了，成功地在主机和虚拟机中安装了git。![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304221249401-1497685917.png)
## 三、Java编写
按照[使用开源中国(码云)托管代码](http://www.cnblogs.com/rocedu/p/5155128.html)教程创建项目并学习[使用码云和博客园学习简易教程](http://www.cnblogs.com/rocedu/p/6482354.html)来上传代码，并编写脚本。![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304220609163-426478085.png)
![](https://images2018.cnblogs.com/blog/1058368/201803/1058368-20180304220612048-221005075.png)
#### 经过了一周的学习，我初步懂得了Linux的操作和Java程序的编写。