public class Example12_1 { 
   public  static void main(String args[]) { //主线程
       SpeakElephant speakElephant = new SpeakElephant();
       SpeakCar speakCar = new SpeakCar();  
       speakElephant.start();                          //启动线程 
       speakCar.start();                         //启动线程
       for(int i=1;i<=15;i++) {
          System.out.print("主人"+i+"  ");
       }  
   }
}

