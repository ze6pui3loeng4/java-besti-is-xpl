public class Example7_2 {
	public static void main(String args[]) {
		ShowBoard board=new ShowBoard();
		board.showMess(new OutputEnglish());
		board.showMess(new OutputAlphabet()
				{
					public void output()
					{
						for(char c='α';c<='ω';c++)
							System.out.printf("%3c",c);
					}
				}
		);
	}
}

