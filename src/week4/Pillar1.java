public class Pillar1 {
	Circle1 bottom;
	double height;
	Pillar1 (Circle1 bottom,double height) {
		this.bottom = bottom;
		this.height=height;
	}
	public double getVolume() {
		return bottom.getArea()*height;
	}
}

