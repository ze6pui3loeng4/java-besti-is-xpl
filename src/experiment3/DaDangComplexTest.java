import junit.framework.TestCase;
import org.testng.annotations.Test;
public class DaDangComplexTest extends TestCase {
    DaDangComplex c1=new DaDangComplex(1,1);
    DaDangComplex c2=new DaDangComplex(1,0);
    DaDangComplex c3=new DaDangComplex(0,1);
    DaDangComplex c4=new DaDangComplex(-1,-1);
    @Test
    public void testComplexAdd() {
        assertEquals("2.0+1.0i",c1.ComplexAdd(c2).toString());
        assertEquals("1.0+1.0i",c2.ComplexAdd(c3).toString());
        assertEquals("1.0+1.0i",c3.ComplexAdd(c2).toString());
        assertEquals("0.0",c1.ComplexAdd(c4).toString());
    }
    @Test
    public void testComplexSub() {
        assertEquals("1.0i",c1.ComplexSub(c2).toString());
        assertEquals("1.0",c1.ComplexSub(c3).toString());
        assertEquals("2.0+2.0i",c1.ComplexSub(c4).toString());
        assertEquals("1.0-1.0i",c2.ComplexSub(c3).toString());
    }
    @Test
    public void testComplexMulti() {
        assertEquals("1.0+1.0i",c1.ComplexMulti(c2).toString());
        assertEquals("-1.0+1.0i",c1.ComplexMulti(c3).toString());
        assertEquals("1.0i",c2.ComplexMulti(c3).toString());
        assertEquals("-2.0i",c1.ComplexMulti(c4).toString());
    }
    @Test
    public void testComplexDiv() {
        assertEquals("1.0+1.0i", c1.ComplexDiv(c2).toString());
        assertEquals("1.0+1.0i", c1.ComplexDiv(c3).toString());
        assertEquals("-1.0-1.0i", c1.ComplexDiv(c4).toString());
        assertEquals("-0.5-0.5i", c2.ComplexDiv(c4).toString());
    }
    @Test
    public void test20165321one() {
        assertEquals("2.0+1.0i", c1.ComplexAdd(c2).toString());
        assertEquals("1.0+2.0i", c1.ComplexAdd(c3).toString());
        assertEquals("0.0", c1.ComplexAdd(c4).toString());
        assertEquals("-1.0i", c2.ComplexAdd(c4).toString());
    }
    @Test
    public void test20165321two() {
        assertEquals("1.0i", c1.ComplexSub(c2).toString());
        assertEquals("1.0", c1.ComplexSub(c3).toString());
        assertEquals("2.0+2.0i", c1.ComplexSub(c4).toString());
        assertEquals("2.0+1.0i", c2.ComplexSub(c4).toString());
    }
    public void test20165321three() {
        assertEquals("1.0+1.0i", c1.ComplexMulti(c2).toString());
        assertEquals("-1.0+1.0i", c1.ComplexMulti(c3).toString());
        assertEquals("-2.0i", c1.ComplexMulti(c4).toString());
        assertEquals("-1.0-1.0i", c2.ComplexMulti(c4).toString());
    }
}
