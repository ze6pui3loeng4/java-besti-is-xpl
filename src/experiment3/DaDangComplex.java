public class DaDangComplex {
    private double RealPart;//复数的实部
    private double ImagePart;//复数的虚部
    public DaDangComplex() {}
    public DaDangComplex(double a, double b) {
        setRealPart(a);
        setImagePart(b);
    }
    public void setRealPart(double a) {
        RealPart = a;
    }
    public void setImagePart(double b) {
        ImagePart = b;
    }
    public double getRealPart() {//返回复数的实部
        return RealPart;
    }
    public double getImagePart() {
        return ImagePart;
    }
    DaDangComplex ComplexAdd(DaDangComplex a) {//(a+bi)+(c+di)=(a+c)+(b+d)i
        DaDangComplex complex = new DaDangComplex();
        complex.RealPart = this.RealPart + a.RealPart;
        complex.ImagePart = this.ImagePart + a.ImagePart;
        return complex;
    }
    DaDangComplex ComplexSub(DaDangComplex a) {//(a+bi)-(c+di)=(a-c)+(b-d)i
        DaDangComplex complex=new DaDangComplex();
        complex.RealPart=this.RealPart-a.RealPart;
        complex.ImagePart=this.ImagePart-a.ImagePart;
        return complex;
    }
    DaDangComplex ComplexMulti(DaDangComplex a) {//(a+bi)*(c+di)=(ac-bd)+(ad+bc)i
        DaDangComplex complex =new DaDangComplex();
        complex.RealPart=this.RealPart*a.RealPart-this.ImagePart*a.ImagePart;
        complex.ImagePart=this.RealPart*a.ImagePart+this.ImagePart*a.RealPart;
        return complex;
    }
    DaDangComplex ComplexDiv(DaDangComplex a) {//(a+bi)/(c+di)=(a+bi)(c-di)/(c^2+d^2)
        DaDangComplex complex=new DaDangComplex();
        complex.RealPart=(this.RealPart*a.ImagePart+this.ImagePart*a.RealPart)/(a.ImagePart*a.ImagePart+a.RealPart*a.RealPart);
        complex.ImagePart=(this.ImagePart*a.ImagePart+this.RealPart*a.RealPart)/(a.ImagePart*a.ImagePart+a.RealPart*a.RealPart);
        return complex;
    }
    @Override
    public String toString() {
        if (ImagePart==0) {
            String str = String.valueOf(RealPart);
            return str;
        }
        else if (RealPart==0) {
            String str = String.valueOf(ImagePart)+"i";
            return  str;
        }
        else {
            if (ImagePart>0) {
                String str = String.valueOf(RealPart) + "+" + String.valueOf(ImagePart) + "i";
                return str;
            }
            else {
                String str = String.valueOf(RealPart) + String.valueOf(ImagePart) + "i";
                return str;
            }
        }
    }
}
