import java.util.*;
public class Diedai {
	public static void main(String args[]) {
		Scanner scanner=new Scanner(System.in);
		System.out.println("请输入m：\n");
		int m=scanner.nextInt();
		System.out.println("请输入n：\n");
		int n=scanner.nextInt();
		int c=ZuHe(m,n);
		System.out.println("C("+m+","+n+")="+c);
	}
	public static int ZuHe(int m,int n) {
		if(m<0||m<n)
			return 0;
		else if(m==n||n==0)
			return 1;
		else if(m>n)
			return ZuHe(m-1,n-1)+ZuHe(m-1,n);
		return -1;
	}
}

