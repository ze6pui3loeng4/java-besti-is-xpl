public class Test {
	public static void main(String args[]){
		CPU cpu =new CPU();//创建一个CPU对象
		cpu.setSpeed(2200);//将cpu的speed设置为2200
		HardDisk disk=new HardDisk();//创建一个HardDisk对象
		disk.setAmount(200);//将disk的amount设置为200
		PC pc=new PC();//创建一个PC对象
		pc.setCPU(cpu);
		pc.setHardDisk(disk);
		pc.show();
	}
}
