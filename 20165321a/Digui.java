public class Digui {
    public static void main(String [] args) {
	    long i;
	    long m=0;
	    long n=Integer.parseInt(args[0]);
	    for(i=1;i<=n;i++) {
		    m=m+fact(i);
	    }
	    System.out.println("1!+2!+…+"+n+"!="+m);
    }
    public static long fact(long n) {
        if (n<=0)
            return 1;
        else
            return n * fact(n-1);
    }
}
